package fr.pixelmon_france.daem0ns.pokedisguise;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import com.pixelmonmod.pixelmon.enums.EnumSpecies;

import net.minecraft.entity.player.EntityPlayerMP;

public interface DisguiseRegistryService {
    boolean hasDisguise(EntityPlayerMP var1, String var2);

    void save(EntityPlayerMP var1);

    void giveDisguise(EntityPlayerMP var1, String var2);
    
    void removeDisguise(EntityPlayerMP var1, String var2);

    default Set<String> getAllDisguises(EntityPlayerMP player) {
        return Arrays.stream(EnumSpecies.values()).map(e -> e.name).filter(n -> this.hasDisguise(player, n)).collect(Collectors.toSet());
    }
}

