package fr.pixelmon_france.daem0ns.pokedisguise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.common.collect.Lists;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class DisguiseCommand  extends CommandBase {
	
	boolean admin = false;
	
    public String getName() {
        return "pokedisguise";
    }

    @Override
    public List<String> getAliases(){
    	return Lists.newArrayList("pdisguise", "disguise");
    }
    
	@Override
	public String getUsage(ICommandSender icommandsender) {
        return "/disguise <player> <specs>";
    }
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
    	if(!sender.canUseCommand(0, "pokedisguise.disguise")) {
            sender.sendMessage(new TextComponentString(TextFormatting.RED+ "You don't have the permission to execute this command!"));
            return;
    	}
    	if(sender instanceof EntityPlayerMP) {
        	EntityPlayerMP player = (EntityPlayerMP)sender;
        	if(args.length==0) {
                player.sendMessage(new TextComponentString(TextFormatting.AQUA+ "Pok�Disguise by "+TextFormatting.AQUA+"Justin!"));
                player.sendMessage(new TextComponentString(TextFormatting.AQUA+ "Ported by "+TextFormatting.RED+"DaeM0nS "+TextFormatting.AQUA+"to forge only!"));
                player.sendMessage(new TextComponentString(TextFormatting.RED+ getUsage(player)));
			}else if(args.length==1) {
        		if(args[0].toLowerCase().equals("off")) {
                    Optional.ofNullable(PokeDisguise.getDisguises().remove(player)).ifPresent(net.minecraft.entity.Entity::setDead);
                    PokeDisguise.getInstance().vphs.show(player);
        			return;
        		}
        		String s = args[0].split(",")[0];
        		
        		if(!EnumSpecies.hasPokemon(s)) {
        			player.sendMessage(new TextComponentString(TextFormatting.RED+"This pokemon ("+s+") doesn't exists."));
        			return;
        		}
        		
            	PokemonSpec spec = new PokemonSpec(args[0].split(","));
                if (PokeDisguise.getService().hasDisguise(player, args[0]) || player.canUseCommand(0, "pokedisguise.all")) {
                    PokeDisguise.disguise(player, spec);
                    return;
                }
                player.sendMessage(new TextComponentString(TextFormatting.RED+ "You don't own this disguise!"));
			}else if(args.length==2) {
        		if(args[0].toLowerCase().equals("off")) {
        			if(!player.canUseCommand(0, "pokedisguise.admin") || !player.getName().equalsIgnoreCase(args[0])) {
                        player.sendMessage(new TextComponentString(TextFormatting.RED+ "You don't have the permission to execute this command!"));
        				return;
        			}
            		if(!Arrays.asList(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()).contains(args[1])) {
                        player.sendMessage(new TextComponentString(TextFormatting.RED+ "This player isn't connected!"));
                        return;
            		}
                    Optional.ofNullable(PokeDisguise.getDisguises().remove(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(args[1]))).ifPresent(net.minecraft.entity.Entity::setDead);
                    PokeDisguise.getInstance().vphs.show(player);
        		}
        		if(!sender.canUseCommand(0, "pokedisguise.other")) {
                    player.sendMessage(new TextComponentString(TextFormatting.RED+ "You can't disguise someone else!"));
                    return;
        		}
        		if(!Arrays.asList(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()).contains(args[0])) {
                    player.sendMessage(new TextComponentString(TextFormatting.RED+ "This player isn't connected!"));
                    return;
        		}
            	PokemonSpec spec = new PokemonSpec(args[1].split(","));
                PokeDisguise.disguise(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(args[0]), spec);
        	}else if (args.length==3) {
    			if(!player.canUseCommand(0, "pokedisguise.admin")) {
                    player.sendMessage(new TextComponentString(TextFormatting.RED+ "You can't modify someone else disguise list!"));
    				return;
    			}
        		if(!Arrays.asList(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerNames()).contains(args[1])) {
                    player.sendMessage(new TextComponentString(TextFormatting.RED+ "This player isn't connected!"));
                    return;
        		}
        		EntityPlayerMP player2 = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(args[1]);
        		if(args[0].toString().toLowerCase().equals("give")) {
        			if(PokeDisguise.getService().hasDisguise(player2, args[3])) {
            	        player.sendMessage(new TextComponentString(TextFormatting.GREEN + "The player already have this disguise"));
        				return;
        			}
        			PokeDisguise.getService().giveDisguise(player2, args[3]);
        	        player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disguise has been given"));
				}else if(args[0].toString().toLowerCase().equals("remove")) {
        			if(PokeDisguise.getService().hasDisguise(player2, args[3])) {
            	        player.sendMessage(new TextComponentString(TextFormatting.GREEN + "The player doesn't have this disguise"));
        				return;
        			}
        			PokeDisguise.getService().removeDisguise(player2, args[3]);
        	        player.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disguise has been removed"));
				}else {
        			player.sendMessage(new TextComponentString(getUsage(player)));
				}
        	}
    	}else {
    		
    	}
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos) {
    	ArrayList<String> s = new ArrayList<String>();
    	s.add("off");
    	EnumSpecies.getNameList().forEach(x -> {s.add(x);});
    	
    	if(sender.canUseCommand(0, "pokedisguise.admin") || sender.canUseCommand(0, "pokedisguise.other")) {
    		if(args.length==1) {
    			return CommandBase.getListOfStringsMatchingLastWord(args, server.getOnlinePlayerNames());
    		}else {
    			return CommandBase.getListOfStringsMatchingLastWord(args, s);
    		}
    	}else {
    		if(args.length==1) {
    			return CommandBase.getListOfStringsMatchingLastWord(args, s);
        	}else {
        		return Collections.emptyList();
        	}
    	}
    }
}