/*
 * Decompiled with CFR 0.146.
 * 
 * Could not load the following classes:
 *  com.flowpowered.math.vector.Vector3d
 *  com.pixelmonmod.pixelmon.api.pokemon.Pokemon
 *  com.pixelmonmod.pixelmon.api.pokemon.PokemonBase
 *  com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec
 *  com.pixelmonmod.pixelmon.client.models.smd.AnimationType
 *  com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue
 *  com.pixelmonmod.pixelmon.enums.EnumBoundingBoxMode
 *  com.pixelmonmod.pixelmon.enums.EnumSpecies
 *  com.pixelmonmod.pixelmon.enums.EnumStatueTextureType
 *  net.minecraft.entity.Entity
 *  net.minecraft.entity.player.EntityPlayerMP
 *  net.minecraft.nbt.NBTBase
 *  net.minecraft.nbt.NBTTagCompound
 *  net.minecraft.network.datasync.DataParameter
 *  net.minecraft.network.datasync.EntityDataManager
 *  net.minecraft.world.World
 *  org.spongepowered.api.Sponge
 *  org.spongepowered.api.command.CommandCallable
 *  org.spongepowered.api.command.CommandException
 *  org.spongepowered.api.command.CommandResult
 *  org.spongepowered.api.command.CommandSource
 *  org.spongepowered.api.command.args.CommandContext
 *  org.spongepowered.api.command.args.CommandElement
 *  org.spongepowered.api.command.args.GenericArguments
 *  org.spongepowered.api.command.spec.CommandExecutor
 *  org.spongepowered.api.command.spec.CommandSpec
 *  org.spongepowered.api.command.spec.CommandSpec$Builder
 *  org.spongepowered.api.data.key.Key
 *  org.spongepowered.api.data.key.Keys
 *  org.spongepowered.api.entity.Entity
 *  org.spongepowered.api.entity.living.player.Player
 *  org.spongepowered.api.event.Listener
 *  org.spongepowered.api.event.game.state.GamePostInitializationEvent
 *  org.spongepowered.api.event.game.state.GamePreInitializationEvent
 *  org.spongepowered.api.event.game.state.GameStoppingServerEvent
 *  org.spongepowered.api.event.network.ClientConnectionEvent
 *  org.spongepowered.api.event.network.ClientConnectionEvent$Disconnect
 *  org.spongepowered.api.plugin.Plugin
 *  org.spongepowered.api.scheduler.Task
 *  org.spongepowered.api.scheduler.Task$Builder
 *  org.spongepowered.api.text.Text
 *  org.spongepowered.api.text.Text$Builder
 *  org.spongepowered.api.text.action.ClickAction
 *  org.spongepowered.api.text.action.HoverAction
 *  org.spongepowered.api.text.action.TextActions
 *  org.spongepowered.api.text.format.TextColor
 *  org.spongepowered.api.text.format.TextColors
 *  org.spongepowered.api.text.serializer.FormattingCodeTextSerializer
 *  org.spongepowered.api.text.serializer.TextSerializers
 *  org.spongepowered.api.world.Location
 *  org.spongepowered.api.world.World
 */
package fr.pixelmon_france.daem0ns.pokedisguise;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import com.pixelmonmod.pixelmon.api.pokemon.PokemonBase;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.client.models.smd.AnimationType;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;
import com.pixelmonmod.pixelmon.enums.EnumBoundingBoxMode;
import com.pixelmonmod.pixelmon.enums.EnumStatueTextureType;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.event.HoverEvent;
import net.minecraft.util.text.event.HoverEvent.Action;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

@Mod(
        modid = "pokedisguise",
        name = "pokedisguise",
        version = "1.0-forge",
        acceptableRemoteVersions = "*",
       	acceptedMinecraftVersions="[1.12.2]",
       	dependencies = "required-after:pixelmon",
       	serverSideOnly = true
)
public class PokeDisguise {
    private static Map<EntityPlayerMP, EntityStatue> disguises = new HashMap<EntityPlayerMP, EntityStatue>();

    int time = 0;
    boolean started=false;
	public PermissionDisguiseRegistryService pdrs;
	public VanishPlayerHideService vphs;
	public static DisguiseRegistryService service;

	private static PokeDisguise instance;

	private static ITextComponent msg;
    
    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent event) {
    	instance = this;
    	pdrs = new PermissionDisguiseRegistryService();
    	vphs = new VanishPlayerHideService();
    	MinecraftForge.EVENT_BUS.register(this);
    }
    
    @Mod.EventHandler
    public void onPostInit(FMLServerStartingEvent event) {
    	service = new PermissionDisguiseRegistryService();
    	event.registerServerCommand(new DisguiseCommand());
    }
    
    @SubscribeEvent
    public void onPlayerLeave(PlayerLoggedOutEvent event) {
        PokeDisguise.removeDisguise(getPlayer(event.player));
        service.save(getPlayer(event.player));
    }

    @Mod.EventHandler
    public void onServerStopping(FMLServerStoppingEvent event) {
        for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
            PokeDisguise.removeDisguise(player);
            service.save(player);
        }
    }

    @Mod.EventHandler
    public void onServerStarted(FMLServerStartedEvent event) throws InterruptedException, ExecutionException, TimeoutException {
    	this.started=true;
    }
    
	@SubscribeEvent
	public void onTick(TickEvent.ServerTickEvent event) {
		if(!started) 
			return;
		if (event.phase == Phase.START)
		{
			getDisguises().forEach((player, statue) -> {
	            if (!((Entity)statue).isEntityAlive()) {
	                PokemonBase base = statue.getPokemon();
	                EnumStatueTextureType type = statue.getTextureType();
	                NBTTagCompound nbt = statue.getEntityData();
	                boolean hidden = statue.getEntityData().getBoolean("hidden");
	                statue = new EntityStatue(player.world);
	                statue.setPokemon(base);
	                statue.setTextureType(type);
	                statue.getEntityData().setBoolean("hidden", hidden);
	                for (String key : nbt.getKeySet()) {
	                    statue.getEntityData().setTag(key, nbt.getTag(key));
	                }
	                PokeDisguise.setupDisguise(statue, player);
	                if (hidden) {
	                    player.removeEntity(statue);
	                }
	                getDisguises().put(player, statue);
	            }
	            BlockPos pos = player.getPosition();
	            statue.moveToBlockPosAndAngles(pos,player.rotationYaw,player.rotationPitch);
	            //statue.setPosition(pos.getX(), pos.getY(), pos.getZ());
	            //statue.setRotation(player.rotationYaw);
	            //Vector3d speed = player.getVelocity().abs();
	            
	            statue.setIsFlying(!player.onGround);
	            if (player.isSprinting() || player.moveForward!=0 || player.moveStrafing!=0) {
	                if (!statue.getAnimation().equals(AnimationType.WALK)) {
	                    statue.setAnimation(AnimationType.WALK);
	                }
	            } else {
	                statue.setAnimation(AnimationType.WALK);
	            }
	        });
			if(time >= 1200) {
	            for (EntityPlayerMP player : FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers()) {
	            	PokeDisguise.service.save(player);
	            }
			}
		}
	}
	


    @SuppressWarnings("deprecation")
	public static void disguise(EntityPlayerMP player, PokemonSpec spec) {
        PokeDisguise.removeDisguise(player);
        EntityStatue statue = new EntityStatue(player.getServerWorld());
        statue.setPokemon(spec.create());
        spec.apply(statue.getEntityData());
        PokeDisguise.setupDisguise(statue, player);
        Optional.ofNullable(getDisguises().put(player, statue)).ifPresent(Entity::setDead);
        
        TextComponentString msg1 = new TextComponentString(TextFormatting.RED+""+TextFormatting.BOLD+"PokeDisguise "+TextFormatting.GRAY+""+TextFormatting.BOLD+">"+TextFormatting.RESET+" Click "+TextFormatting.GREEN+"HERE"+TextFormatting.RESET+" to hide your disguise");
        msg1.getStyle().setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new TextComponentString("Hiding your disguise makes it so others can see it but you can't")));
        
        TextComponentString msg2 = new TextComponentString(TextFormatting.RED+""+TextFormatting.BOLD+"PokeDisguise "+TextFormatting.GRAY+""+TextFormatting.BOLD+">"+TextFormatting.RESET+" Click "+TextFormatting.GREEN+"HERE"+TextFormatting.RESET+" to unhide your disguise");
        msg2.getStyle().setHoverEvent(new HoverEvent(Action.SHOW_TEXT, new TextComponentString("Unhiding your disguise makes it so you can see it")));
        
        msg = TextUtils.addCallback(msg1, sender1 -> {
            disguises.get(player).getEntityData().setBoolean("hidden", true);
            player.removeEntity(disguises.get(player));
            ITextComponent msgg = TextUtils.addCallback(msg2, sender2 -> {
                disguises.get(player).getEntityData().setBoolean("hidden", false);
                disguises.get(player).setDead();
                player.sendMessage(msg);
            });
            player.sendMessage(msgg);
        });

        /*
        AtomicReference<String> message = new AtomicReference<String>();
        message.set(Text.builder().append(new Text[]{TextSerializers.FORMATTING_CODE.deserialize("&4&lPokeDisguise &7&l>&r Click &aHERE&r to hide your disguise")}).onHover((HoverAction)TextActions.showText((Text)Text.of((Object[])new Object[]{TextColors.WHITE, "Hiding your disguise makes it so others can see it but you can't"}))).onClick((ClickAction)TextActions.executeCallback(src -> {
            disguises.get((Object)player).getEntityData().setBoolean("hidden", true);
            player.removeEntity(disguises.get(player));
            player.sendMessage(Text.builder().append(new Text[]{TextSerializers.FORMATTING_CODE.deserialize("&4&lPokeDisguise &7&l>&r Click &aHERE&r to unhide your disguise")}).onHover((HoverAction)TextActions.showText((Text)Text.of((Object[])new Object[]{TextColors.WHITE, "Unhiding your disguise makes it so you can see it"}))).onClick((ClickAction)TextActions.executeCallback(src1 -> {
                disguises.get((Object)player).getEntityData().setBoolean("hidden", false);
                disguises.get((Object)player).setDead();
                player.sendMessage((Text)message.get());
            })).build());
        })).build());*/
        player.sendMessage(msg);
    }

    public static void removeDisguise(EntityPlayer player) {
        Optional.ofNullable(getDisguises().remove(player)).ifPresent(entityStatue -> {
            entityStatue.setDead();
            PokeDisguise.getInstance().vphs.show((EntityPlayerMP) player);
        });
    }

    @SuppressWarnings("unchecked")
	private static void setupDisguise(EntityStatue statue, EntityPlayerMP player) {
        statue.setPosition(player.lastTickPosX, player.lastTickPosY, player.lastTickPosZ);
        try {
            Field dwBox = EntityStatue.class.getDeclaredField("dwBoundMode");
            dwBox.setAccessible(true);
            statue.getDataManager().set((DataParameter<Byte>)dwBox.get(null), (byte)EnumBoundingBoxMode.None.ordinal());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        statue.setAnimation(AnimationType.WALK);
        statue.setAnimate(true);
        PokeDisguise.getInstance().vphs.hide(player);
        player.world.spawnEntity(statue);
    }
    
    public EntityPlayerMP getPlayer(EntityPlayer player) {
    	return Optional.of(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(player.getUniqueID())).isPresent()?FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(player.getUniqueID()):(EntityPlayerMP)player;
    }

	public static DisguiseRegistryService getService() {
		return service;
	}

	public void setService(DisguiseRegistryService service) {
		PokeDisguise.service = service;
	}

	public static Map<EntityPlayerMP, EntityStatue> getDisguises() {
		return disguises;
	}

	public static void setDisguises(Map<EntityPlayerMP, EntityStatue> disguises) {
		PokeDisguise.disguises = disguises;
	}
	
	public static PokeDisguise getInstance() {
		return instance;
	}
	
}

